from datetime import datetime

from .base import db


class Message(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    chat_id = db.Column(db.Integer, db.ForeignKey('chats.id'), nullable=False)
    author_id = db.Column(db.Integer, nullable=False)
    text = db.Column(db.Text, nullable=False, default='', server_default='')
    created_at = db.Column(db.DateTime, default=datetime.now)

    chat = db.relationship('Chat', back_populates='messages')

    __tablename__ = 'messages'
